
//import com.tmb.me.mepay.service.db.AccountStatementDAO;
//import junit.framework.Test;
//import junit.framework.TestCase;
//import junit.framework.TestSuite;
//import oracle.jdbc.pool.OracleDataSource;
//import org.joda.time.DateTime;
//import org.joda.time.Days;
//import org.joda.time.LocalDate;
//import org.joda.time.Months;
//import org.joda.time.format.DateTimeFormat;
//import org.joda.time.format.DateTimeFormatter;
//
//import javax.sql.DataSource;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Properties;
//import java.util.concurrent.TimeUnit;
//
///**
// * Unit test for simple App.
// */
public class AppTest4 
  //  extends TestCase
{
//    /**
//     * Create the test case
//     *
//     * @param testName name of the test case
//     */
//    public AppTest( String testName )
//    {
//        super( testName );
//    }
//
//    /**
//     * @return the suite of tests being tested
//     */
//    public static Test suite()
//    {
//        return new TestSuite( AppTest.class );
//    }
//
//    /**
//     * Rigourous Test :-)
//     */
//    public void testApp()
//    {
//        assertTrue( true );
//    }
//
//    public void testDateCalculation(){
//
//        AccountStatementDAO accountStatementDAO=new AccountStatementDAO();
//        accountStatementDAO.setDs(getOracleDataSource());
//
//        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
//        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MMM-yyyy");
//        String dat1 = "4-Jan-2017";
//        String dat2 = "28-Mar-2017";
//
//        String firstDate="01-Jan-2010";
//
//        try {
//            DateTime start = dtf.parseDateTime(dat1);
//            DateTime end = dtf.parseDateTime(dat2);
//
//            DateTime backendLastDate = new DateTime();
//            DateTime backendStartDate=backendLastDate.minusMonths(6).withDayOfMonth(1).withTimeAtStartOfDay();
//
//            System.out.println(" Backend Start Date ---> "+backendStartDate);
//            System.out.println("Backend Last Date ---> "+ backendLastDate);
//
//            DateTime custSavingStatementLastDate = backendLastDate.withDayOfMonth(1).minusMonths(6).minusDays(1).millisOfDay().withMaximumValue();
//            DateTime custSavingStatementStartDate = backendLastDate.withDayOfMonth(1).minusMonths(36).withTimeAtStartOfDay();
//
//            System.out.println(" Cust Saving Statement Start Date ---> "+custSavingStatementStartDate);
//            System.out.println(" Cust Saving Statement Last Date ---> "+ custSavingStatementLastDate);
//
//            DateTime custSavingStatementArchStartDate = dtf.parseDateTime(firstDate).withTimeAtStartOfDay();
//            DateTime custSavingStatementArchLastDate = custSavingStatementStartDate.minusDays(1).millisOfDay().withMaximumValue();
//
//            System.out.println(" Cust Saving Statement Arch Start Date ---> "+custSavingStatementArchStartDate);
//            System.out.println(" Cust Saving Statement Arch Last Date ---> "+ custSavingStatementArchLastDate);
//
//
//           if( backendStartDate.compareTo(start) * start.compareTo(backendLastDate) >= 0 ){
//               System.out.println("Start date confirms to check only in service");
//               System.out.println("End date must falls in Service Only");
//               //logics to search only on service
//
//
//
//           }else if( custSavingStatementStartDate.compareTo(start) * start.compareTo(custSavingStatementLastDate) >= 0){
//
//               System.out.println("Start date falls in Cust Savings Statement");
//
//               //check if end date falls in same or not
//               if(custSavingStatementStartDate.compareTo(end) * end.compareTo(custSavingStatementLastDate) >= 0){
//                   System.out.println("End date falls in Cust Savings Statement");
//                   //logics to search only on CustSavingsStatements
//
//               }else{
//                   System.out.println("End date falls in Service");
//                   //logics to search on both CustSavingsStatements and Service
//
//               }
//
//
//           }else if( custSavingStatementArchStartDate.compareTo(start) * start.compareTo(custSavingStatementArchLastDate) >= 0){
//
//               System.out.println("Start date falls in Cust Savings Statement Archives");
//
//               //check if end date falls in same or not
//               if(custSavingStatementArchStartDate.compareTo(end) * end.compareTo(custSavingStatementArchLastDate) >= 0){
//                   System.out.println("End date falls in Cust Savings Statement Arch");
//                   //logic to search only on custSavingsStatementArch Table
//
//               }else if(custSavingStatementStartDate.compareTo(end) * end.compareTo(custSavingStatementLastDate) >= 0){
//                   System.out.println("End date falls in  Cust Savings Statement");
//                   //logic to search on custSavingsStatementArch Table and also custSavingsStatement table
//
//               }else{
//                   System.out.println("Calculation also requires to find full Cust Savings Statement");
//                   System.out.println("End date falls in Service");
//
//                   //logic to search on custSavingsStatementArch Table and custSavingsStatement table and also with service
//
//               }
//
//
//           }
//
//            System.out.println("Total Size of records "+ accountStatementDAO.getCustAccountStatement("custSavingStatment","9202116555","01-JAN-13","7-OCT-17").size());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        /* System.out.println("Output here");
//        Date d1=new Date();*/
//        assertTrue(true);
//
//    }
//
//    private  DataSource getOracleDataSource(){
//
//        OracleDataSource oracleDS = null;
//        try {
//            oracleDS = new OracleDataSource();
//            oracleDS.setURL("jdbc:oracle:thin:@13.76.35.171:1521/tmbdev");
//            oracleDS.setUser("CRMADM");
//            oracleDS.setPassword("tmb##1234");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return oracleDS;
//    }
}
